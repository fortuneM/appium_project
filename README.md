#Universal Music Player Automation Test Suite

##Pre-requisites:
IntelliJ,
Java 8^,
Maven 3,
Appium and Android Emulator

###Tests are designed on Cucumber BDD framework to give easy understanding to Stakeholders on the scenarios being executed.

### Reports are generated on a Cucumber Report link: e.g https://reports.cucumber.io/reports/ea1da29a-f57b-43cc-afe9-952ef1bacf2d

#To run this test use IntelliJ Run or execute from AppiumTestRunner class

