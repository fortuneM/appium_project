@MusicPlayer
Feature: Test Universal Music Player application functionality

  @PlayMusic
  Scenario Outline: <scenarioname>
    Given As a user i launch Universal Music Player application
    And I select the Genre
    And I select genre type "<genre>"
    When I select the song on list and play
    Then I can pause the music

    Examples:
      | scenarioname         | genre |
      | Play music and pause | Rock  |