package PageObject;

import Common.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.swing.*;
import java.util.concurrent.TimeUnit;

public class MusicPlayerPage extends BasePage {

//    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout")
//    private MobileElement lblGenreList;

//    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]")
//    private MobileElement lblGenreTypeList;

//    @AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc=\"Play item\"])[1]")
//    private MobileElement lblSongSelectList;

//    @AndroidFindBy( id = "play or pause")
//    private MobileElement btnPlayPause;

    public MusicPlayerPage(WebDriver driver) {
        super();
    }

    /**
     * Description: This method Click Genre on the list
     */
    public boolean clickOnGenre() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement lblGenreList = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout"));
//        waitPageLoad();
        try {
            lblGenreList.click();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * Description: This method clicks on Genre type list
     */
    public boolean clickGenreTypeName() {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement lblGenreTypeList =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]"));
        try {
            lblGenreTypeList.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Description: This method clicks on the song to play
     */
    public boolean clickSongandPlay() {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement lblSongSelectList = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]"));
        try {
            lblSongSelectList.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Description: This method clicks on the song to pause or play
     */
    public boolean clickPausePlaySong() {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement btnPlayPause = driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"play or pause\"]\n"));
        try {
            btnPlayPause.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
