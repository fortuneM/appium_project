package Common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BasePage {

    public static WebDriver driver;

    public void openMusicPlayer() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        File appDir = new File("src/test/resources");
        File app = new File(appDir, "UAMPMusicPlayerApp.apk");
        capabilities.setCapability("BROWSER_NAME", "Android");
        capabilities.setCapability("VERSION", "30");
        capabilities.setCapability("deviceName", "Pixel_3a_API_30");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("app", app.getAbsolutePath());

        capabilities.setCapability("appPackage", "com.instantappsample.uamp");

        capabilities.setCapability("appActivity", "com.example.android.uamp.ui.MusicPlayerActivity"); // This is Launcher activity of your app (you can get it from apk info app)

        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

    }

    public void waitPageLoad() {
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    }

}

