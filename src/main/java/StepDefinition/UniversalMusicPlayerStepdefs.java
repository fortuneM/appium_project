package StepDefinition;

import Common.BasePage;
import PageObject.MusicPlayerPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.net.MalformedURLException;

public class UniversalMusicPlayerStepdefs extends BasePage {

    MusicPlayerPage musicPlayerPage = new MusicPlayerPage(driver);

    @Given("As a user i launch Universal Music Player application")
    public void asAUserILaunchSwaglabsApplication() throws MalformedURLException {
        System.out.println("Opening Browser and launching application...");
        openMusicPlayer();
    }

    @Given("I select the Genre")
    public void i_select_the_genre() {
        Assert.assertTrue("Unable to click on Music genre", musicPlayerPage.clickOnGenre());
    }

    @Given("I select genre type {string}")
    public void i_select_genre_type(String genName) {
        Assert.assertTrue("Unable to click on Music genre type name", musicPlayerPage.clickGenreTypeName());
    }

    @When("I select the song on list and play")
    public void i_select_the_song_on_list_and_play() {
        Assert.assertTrue("Unable to click on song and play ", musicPlayerPage.clickSongandPlay());
    }

    @Then("I can pause the music")
    public void i_can_pause_the_music() {
        Assert.assertTrue("Unable to click on pause track", musicPlayerPage.clickPausePlaySong());
    }

}
